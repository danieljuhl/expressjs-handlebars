Express.js 4.x Handlebars
=========================

[Handlebars][] view engine for [Express][] 4.x.

[Express]: https://github.com/visionmedia/express
[Handlebars]: https://github.com/wycats/handlebars.js


