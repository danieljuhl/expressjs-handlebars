'use strict';

var HandlebarsEngine = require('./lib/engine');

function expresshandlebars(config) {
  return expresshandlebars.create(config).engine;
}

expresshandlebars.create = function (config) {
  return new HandlebarsEngine(config);
};

expresshandlebars.HandlebarsEngine = HandlebarsEngine;

module.exports = expresshandlebars;
