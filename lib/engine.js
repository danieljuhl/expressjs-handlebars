'use strict';

// ----------------------------------------
// Notes and TODOs
// ----------------------------------------
/*
  - Minify using handlebars-helper-minify
  - Remove or re-write all code not written by DJ
  - Create tests
  - Create examples
*/

// ----------------------------------------
// Required modules
// ----------------------------------------

var fs     = require('fs');
var path   = require('path');
var async  = require('async');
var _      = require('underscore');
var glob   = require('glob');
var semver = require('semver');

// ----------------------------------------
// Utilities (private)
// ----------------------------------------

function extractSemver(object){
  var version = object.VERSION || '';

  // If not a valid semver, convert it
  if (version && !semver.valid(version)) {
    // x.x.string
    version = version.replace(/(\d\.\d)\.(\D.*)/, '$1.0-$2');
  }

  return version;
}

// ----------------------------------------
// Variables (private)
// ----------------------------------------

var _cache        = {};
var _dirCache     = {};
var _fileCache    = {};
var _pendingReads = {};

// ----------------------------------------
// Constructor
// ----------------------------------------

function HandlebarsEngine(config){
  config = config || {};

  _.extend(this, _.pick(config, [
    'layoutsDir',
    'partialsDir',
    'extname',
    'defaultLayout'
  ]));

  _.defaults(this, {
    layoutsDir: 'layouts/',
    partialsDir: 'partials/',
    extname: '.handlebars'
  });

  if (this.extname[0] !== '.') {
    this.extname = '.' + this.extname;
  }

  var handlebars = config.handlebars || require('handlebars');
  this.handlebars = handlebars;
  this.handlebarsVersion = extractSemver(handlebars);

  this.compiled    = {};
  this.precompiled = {};

  if ('helpers' in config) {
    this.registerHelpers(config.helpers);
  }

  this.engine = this.renderView.bind(this); // todo
}


// -- Statics ------------------------------------------------------------------

HandlebarsEngine._dirCache     = {};
HandlebarsEngine._fileCache    = {};
HandlebarsEngine._pendingReads = {};


// ----------------------------------------
// Prototype
// ----------------------------------------

_.extend(HandlebarsEngine.prototype, {

  // --- Public Methods -------------------

  // registerHelper
  registerHelper: function(name, fn){
    if (this.handlebars && this.handlebars.registerHelper) {
      this.handlebars.registerHelper(name, fn);
    }
  },

  registerHelpers: function(helpers){
    var self = this;
    // validate that helpers is an object
    _.each(helpers, function(fn, name){
      self.registerHelper(name, fn);
    });
  },

  registerPartial: function(name, value){

  },

  registerPartials: function(partials){
    // if partials is a path, load all partials from that path
    // else register each partial if partials is an object
  },

  // ------

  loadPartials: function (options, callback) {
    var self = this;

    if (arguments.length < 2 && typeof options === 'function') {
      callback = options;
      options  = {};
    }

    options = options || {};

    function load(dirs, options, callback) {
      if (!_.isArray(dirs)) {
        dirs = [dirs];
      }
      var loadTemplates = self.loadTemplates.bind(self);

      async.map(dirs, function (dir, callback) {
        loadTemplates(dir, options, callback);
      }, callback);
    }

    function mapPartials(dirs, callback) {
      var getPartialName = self._getPartialName.bind(self);
      var partials;

      partials = dirs.reduce(function (partials, templates) {
        Object.keys(templates).forEach(function (filePath) {
          partials[getPartialName(filePath)] = templates[filePath];
        });

        return partials;
      }, {});

      callback(null, partials);
    }

    async.waterfall([
      load.bind(self, self.partialsDir, options),
      mapPartials.bind(self)
    ], callback);
  },

  loadTemplate: function (filePath, options, callback) {
    filePath = path.resolve(filePath);

    if (arguments.length < 3 && typeof options === 'function') {
      callback = options;
      options  = {};
    }

    options = options || {};

    var precompiled = options.precompiled,
      cache       = precompiled ? this.precompiled : this.compiled,
      template    = options.cache && cache[filePath],
      compile;

    if (template) {
      callback(null, template);
      return;
    }

    compile = this.handlebars[precompiled ? 'precompile' : 'compile'];

    this._loadFile(filePath, options, function (err, file) {
      if (err) { return callback(err); }

      try {
        template = cache[filePath] = compile(file);
        callback(null, template);
      } catch (err) {
        callback(err);
      }
    });
  },

  loadTemplates: function (dirPath, options, callback) {
    var self = this;

    if (arguments.length < 3 && typeof options === 'function') {
      callback = options;
      options  = {};
    }

    options = options || {};

    var viewsPath = options.settings && options.settings.views || '';

    function load(filePath, callback) {
      self.loadTemplate(path.join(viewsPath, dirPath, filePath), options, callback);
    }

    function mapTemplates(filePaths, callback) {
      async.map(filePaths, load.bind(self), function (err, templates) {
        if (err) { return callback(err); }

        var map = filePaths.reduce(function (map, filePath, i) {
          map[filePath] = templates[i];
          return map;
        }, {});

        callback(null, map);
      });
    }

    async.waterfall([
      self._loadDir.bind(self, path.join(viewsPath, dirPath), options),
      mapTemplates.bind(self)
    ], callback);
  },

  render: function(path, options, callback){
    var self = this;

    if (arguments.length < 3 && typeof options === 'function') {
      callback = options;
      options  = {};
    }

    options = options || {};

    var helpers = _.extend({}, this.handlebars.helpers, this.helpers, options.helpers);

    function loadTemplates(callback) {
      async.parallel({
        partials: self.loadPartials.bind(self, options),
        template: self.loadTemplate.bind(self, path, options)
      }, callback);
    }

    function renderTemplate(templates, callback) {
      self._renderTemplate(templates.template, options, {
        helpers : helpers,
        partials: templates.partials
      }, callback);
    }

    // Force `{precompiled: false}` option, before passing `options` along
    // to `getPartials()` and `getTemplate()` methods.
    if (options.precompiled) {
      options = _.extend({}, options, {precompiled: false});
    }

    async.waterfall([
      loadTemplates.bind(self),
      renderTemplate.bind(self)
    ], callback);
  },

  renderView: function(path, options, callback){
    var self = this;

    if (arguments.length < 3 && typeof options === 'function') {
      callback = options;
      options  = {};
    }

    options = options || {};

    var layoutPath = this._resolveLayoutPath(options);

    function renderLayout(body, callback) {
      var context = _.extend({}, options, {body: body});
      self.render(layoutPath, context, callback);
    }

    // Simple render when no layout is used.
    if (!layoutPath) {
      this.render.apply(this, arguments);
      return;
    }

    // Force `{precompiled: false}` option, before passing options along to
    // `getPartials()` and `getTemplate()` methods.
    if (options.precompiled) {
      options = _.extend({}, options, {precompiled: false});
    }

    async.waterfall([
      self.render.bind(self, path, options),
      renderLayout.bind(self)
    ], callback);

  },

  // --- Private Methods ------------------

  _getPartialName: function (filePath) {
    var extRegex = new RegExp(this.extname + '$'),
      name     = filePath.replace(extRegex, ''),
      version  = this.handlebarsVersion;

    // Fixes a Handlebars bug in versions prior to 1.0.rc.2 which caused
    // partials with "/"s in their name to not be found.
    // https://github.com/wycats/handlebars.js/pull/389
    if (version && !semver.satisfies(version, '>=1.0.0-rc.2')) {
      name = name.replace('/', '.');
    }

    return name;
  },

  _loadDir: function(dirPath, options, callback){
    dirPath = path.resolve(path.join(dirPath));

    var dirCache     = HandlebarsEngine._dirCache,
        pendingReads = HandlebarsEngine._pendingReads,
        dir          = options.cache && dirCache[dirPath],
        callbacks, pattern;

    if (dir) {
      callback(null, dir.concat());
      return;
    }

    callbacks = pendingReads[dirPath];

    if (callbacks) {
      callbacks.push(callback);
      return;
    }

    callbacks = pendingReads[dirPath] = [callback];
    pattern   = '**/*' + this.extname;

    glob(pattern, {cwd: dirPath}, function (err, dir) {
      if (!err) {
        dirCache[dirPath] = dir;
      }

      while (callbacks.length) {
        callbacks.shift().call(null, err, dir && dir.concat());
      }

      delete pendingReads[dirPath];
    });
  },

  _loadFile: function(filePath, options, callback){
    filePath = path.resolve(filePath);

    var fileCache    = HandlebarsEngine._fileCache,
      pendingReads = HandlebarsEngine._pendingReads,
      file         = options.cache && fileCache[filePath],
      callbacks;

    if (file) {
      callback(null, file);
      return;
    }

    callbacks = pendingReads[filePath];

    if (callbacks) {
      callbacks.push(callback);
      return;
    }

    callbacks = pendingReads[filePath] = [callback];

    fs.readFile(filePath, 'utf8', function (err, file) {
      if (!err) {
        fileCache[filePath] = file;
      }

      while (callbacks.length) {
        callbacks.shift().call(null, err, file);
      }

      delete pendingReads[filePath];
    });
  },

  _renderTemplate: function(template, context, options, callback){
    try {
      var output = template(context, options);
      callback(null, output);
    } catch (err) {
      callback(err);
    }
  },

  _resolveLayoutPath: function (options) {
    // Makes sure to interpret falsy `options.layout` values as no layout.
    var layoutPath = 'layout' in options ? options.layout : this.defaultLayout;

    if (!layoutPath) {
      return null;
    }

    var viewsPath = options.settings && options.settings.views || '';

    if (!path.extname(layoutPath)) {
      layoutPath += this.extname;
    }

    if (layoutPath[0] !== '/') {
      layoutPath = path.join(viewsPath, this.layoutsDir, layoutPath);
    }

    return layoutPath;
  },

  _readFile: function(path, options, callback){
    var _cache = HandlebarsEngine._fileCache;

    // if caching is enabled and file has already been cached, serve it directly
    if (options.cache === true && _cache[path]) {
      return callback(null, _cache[path]);
    }


    fs.readFile(path, 'utf8', function(err, content){
      // if caching is enabled, cache the file content
      if (!err && options.cache === true) {
        _cache[path] = content;
      }

      callback(err, content);
    });
  }

});


// ----------------------------------------
// Exports
// ----------------------------------------

module.exports = HandlebarsEngine;
